/*
 * vim: sw=2 ts=2 expandtab
 *
 * Copyright (c) 2013-2019 Thomas Kolb
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <malloc.h>
#include <errno.h>

#include "util.h"
#include "logger.h"

void remove_trailing_slash(char *str) {
  size_t offset = strlen(str)-1;
  if((offset != 0) && (str[offset] == '/')) {
    str[offset] = '\0';
  }
}

void urlencode(const char *str, char *result) {
  uint8_t c;
  while(*str) {
    c = *str;

    if((c >= 'a' && c <= 'z') ||
        (c >= 'A' && c <= 'Z') ||
        (c >= '0' && c <= '9') ||
        c == '-' ||
        c == '_' ||
        c == '!' ||
        c == '(' ||
        c == ')' ||
        c == '$' ||
        c == '*' ||
        c == '\'' ||
        c == ',' ||
        c == '.') {
      // simply print safe characters
      result += sprintf(result, "%c", c);
    } else {
      // encode all others
      result += sprintf(result, "%%%02X", c);
    }

    str++;
  }
}

/*!
 * Append src to target, reallocating target on the way if necessary.
 *
 * \param target     The string to append to.
 * \param targetsize A pointer to the size of target. Will be updated by this
 *                   function when target is reallocated.
 * \param src        The string to append to target.
 * \returns          A pointer to new string (target becomes invalid in the
 *                   event of reallocation) or NULL on a realloc error.
 */
char* safe_append(char *target, size_t *targetsize, const char *src) {
  size_t targetlen = strlen(target);
  size_t srclen = strlen(src);
  size_t newsize = 2*targetlen + srclen + 1;

  // check if reallocation is necessary
  if((targetlen + srclen) >= *targetsize) {
    LOG(LVL_DEBUG, "safe_append: reallocating target string: %lu -> %lu characters", *targetsize, newsize);

    target = realloc(target, newsize * sizeof(char));
    if(target == NULL) {
      LOG(LVL_ERR, "safe_append: realloc failed: %s", strerror(errno));
      return NULL;
    }

    *targetsize = newsize;
  }

  return strcat(target, src);
}
