/*
 * vim: sw=2 ts=2 expandtab
 *
 * Copyright (c) 2013-2019 Thomas Kolb
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MAIN_H
#define MAIN_H

#include <limits.h>

#define IPSTR_MAXLEN 47

enum ResultCode {
  RC_OK = 0,                  // No problem (so far)
  RC_EXISTS = 1,              // On upload, the file to be uploaded already existed
  RC_OPEN_FAILED = 2,         // On upload, opening the file for writing failed
  RC_WRONG_TARGET = 3,        // On upload, the given URL was not a directory
  RC_WRITE_FAILED = 4         // On upload, a write operation to the file failed
};

enum RequestType {
  GET,
  POST
};

struct ConnectionState {
  char cleanedURL[PATH_MAX];
  char localFileName[PATH_MAX];
  struct stat targetStat;

  uint8_t uploadRequest;

  FILE *upload_fd;
  char uploadFilename[PATH_MAX];

  char clientIP[IPSTR_MAXLEN];

  enum ResultCode result;
  enum RequestType requestType;

  struct MHD_PostProcessor *postProcessor;
};

struct RequestRange {
  off_t start;
  off_t length;
};


#endif // MAIN_H
